package com.systems.cyberduck.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MyViewAdapter extends RecyclerView.Adapter<MyViewAdapter.MyViewHolder> {
    private final List<String> personData;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView cardTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            cardTextView = ((TextView)itemView.findViewById(R.id.textView));
        }

        public void assignData(String lecturer){
            this.cardTextView.setText(lecturer);
        }
    }




    public MyViewAdapter(List<String> personData){
        this.personData = personData;
    }
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup,
                false);
        return new MyViewHolder(v);
    }
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.assignData(personData.get(position));
    }
    public int getItemCount() {
        return personData.size();
    }
}
